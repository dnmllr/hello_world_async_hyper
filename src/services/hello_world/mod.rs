use hyper;
use hyper::{Get, StatusCode};
use hyper::header::ContentLength;
use hyper::server::{Http, Service, Request, Response};
use std::net;
use rand;
use rand::Rng;
use time;

use slog;

fn gen_marker() -> String {
    let mut rng = rand::thread_rng();
    format!("{}", rng.gen::<u64>())
}

pub struct HelloWorld {
    log: slog::Logger,
    msg: &'static str,
}

impl Service for HelloWorld {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;
    type Future = ::futures::Finished<Response, hyper::Error>;

    fn call(&self, req: Request) -> Self::Future {
        let marker = gen_marker();
        let log = self.log.new(o!("request_marker" => marker));
        info!(log, "Servicing request"; "started_at" => format!("{}", time::now().rfc3339()));
        ::futures::finished(match (req.method(), req.path()) {
            (&Get, "/") | (&Get, "/hello") => {
                info!(log, "Replying"; "finished_at" => format!("{}", time::now().rfc3339()));
                Response::new()
                    .with_header(ContentLength(self.msg.len() as u64))
                    .with_body(self.msg)
            }
            _ => Response::new().with_status(StatusCode::NotFound),
        })
    }
}

impl HelloWorld {
    pub fn new(msg: &'static str, logger: &slog::Logger) -> HelloWorld {
        let marker = gen_marker();
        HelloWorld {
            log: logger.new(o!("connection_marker" => marker)),
            msg: msg,
        }
    }

    pub fn start(port: u16, msg: &'static str, logger: &slog::Logger) -> hyper::Result<()> {
        let log = logger.new(o!("service" => "hello_world", "started_at" => format!("{}", time::now().rfc3339())));

        info!(log, "starting service hello world with parameters";
              "port" => format!("{}", port), "message" => msg);

        let addr = net::SocketAddr::V6(net::SocketAddrV6::new(net::Ipv6Addr::new(0,
                                                                                 0,
                                                                                 0,
                                                                                 0,
                                                                                 0,
                                                                                 0,
                                                                                 0,
                                                                                 0x1),
                                                              port,
                                                              0,
                                                              0));
        let server = Http::new().bind(&addr, move || Ok(HelloWorld::new(msg, &log)))?;
        server.run()
    }
}
