RUST=cargo
PROD_FLAGS=--release --target=x86_64-unknown-linux-musl

default:
				$(RUST) build $(PROD_FLAGS)

run-default: export PORT = 8080
run-default: export APP_NAME = "Hello Test"
run-default: export BUILD_ID = "1"
run-default: export VERSION = "0.0.1"
run-default:
				$(RUST) run

run-prod: export RUST_LOG=error
run-prod: export PORT = 8080
run-prod: export APP_NAME = "Hello Test"
run-prod: export BUILD_ID = "1"
run-prod: export VERSION = "0.0.1"
run-prod:
				$(RUST) run $(PROD_FLAGS)

run-profile: export RUST_LOG=error
run-profile: export PORT = 8080
run-profile: export APP_NAME = "Hello Test"
run-profile: export BUILD_ID = "1"
run-profile: export VERSION = "0.0.1"
run-profile:
				$(RUST) build $(PROD_FLAGS)
				valgrind --tool=callgrind --dump-instr=yes --collect-jumps=yes --simulate-cache=yes ./target/x86_64-unknown-linux-musl/release/distributed

