#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![deny(warnings)]

extern crate hyper;
extern crate serde;
extern crate serde_json;
extern crate clap;
extern crate futures;
#[macro_use]
extern crate slog;
extern crate slog_term;
extern crate time;
extern crate rand;

mod configuration;
mod services;

use slog::DrainExt;
use std::process;

use configuration::Configuration;
use services::hello_world::HelloWorld;

fn init() -> (Configuration, slog::Logger) {
    match Configuration::new() {
        Ok(config) => {
            // TODO(dan) figure out if there is a more elegant way to do this rather than
            // cloning the config to pass it into the logger.
            let Configuration { application_name, build_id, port, version } = config.clone();
            let root_logger =
                slog::Logger::root(slog_term::streamer().async().full().build().fuse(),
                                   o!("name" => application_name,
                                                    "on_port" => port,
                                                    "version" => version,
                                                    "build_id" => build_id,
                                                    ));
            info!(root_logger, "Application started"; "started_at" => format!("{}", time::now().rfc3339()));
            (config, root_logger)
        }
        Err(e) => {
            let root_logger = slog::Logger::root(slog_term::streamer().build().fuse(),
                                                 o!("failed" => "true"));
            error!(root_logger, "Application failed to start";
                   "attempted_at" => format!("{}", time::now().rfc3339()),
                   "reason" => format!("{}", e));
            process::exit(1);
        }
    }
}

fn main() {
    let (config, log) = init();
    info!(log, "onwards and upwards"; "configuration" => format!("{}", config));
    HelloWorld::start(config.port, "hello world", &log).unwrap();
}
