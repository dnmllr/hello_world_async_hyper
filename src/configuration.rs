use std::env;
use std::error::Error;
use std::fmt;
use std::ffi::OsString;
use std::num::ParseIntError;

#[derive(Debug, Eq, PartialEq)]
pub enum EnvVarLookupError {
    InvalidEnvVariable(&'static str, OsString),
    InvalidPortNumber(ParseIntError),
    PortNumberNotProvided,
    AppNameNotProvided,
    BuildIdNotProvided,
    VersionNotProvided,
}

impl fmt::Display for EnvVarLookupError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            EnvVarLookupError::InvalidEnvVariable(name, _) => {
                write!(f, "Environment variable {} was not valid utf8", name)
            }
            EnvVarLookupError::InvalidPortNumber(ref e) => {
                write!(f,
                       "Environment variable PORT was not parsable as u16: {}",
                       e)
            }
            EnvVarLookupError::PortNumberNotProvided |
            EnvVarLookupError::AppNameNotProvided |
            EnvVarLookupError::BuildIdNotProvided |
            EnvVarLookupError::VersionNotProvided => write!(f, "{}", self.description()),
        }
    }
}

impl Error for EnvVarLookupError {
    fn description(&self) -> &str {
        match *self {
            EnvVarLookupError::InvalidEnvVariable(_, _) => {
                "Environment variable was not valid utf8"
            }
            EnvVarLookupError::InvalidPortNumber(_) => {
                "Provided PORT environment variable couldn't be parsed as u16"
            }
            EnvVarLookupError::PortNumberNotProvided => "PORT environment variable was not set",
            EnvVarLookupError::AppNameNotProvided => "APP_NAME environment variable was not set",
            EnvVarLookupError::BuildIdNotProvided => "BUILD_ID environment variable was not set",
            EnvVarLookupError::VersionNotProvided => "VERSION environment variable was not set",
        }
    }

    fn cause(&self) -> Option<&Error> {
        // TODO(dan) Do something better here
        None
        // match *self {
        // InvalidEnvVariable(_, OsString) => Some(&env::VarError::NotUnicode(OsString)),
        // PortNumberNotProvided | AppNameNotProvided | BuildIdNotProvided |
        // VersionNotProvided => Some(&env::VarError::NotPresent),
        // }
        //
    }
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct Configuration {
    pub port: u16,
    pub application_name: String,
    pub build_id: String,
    pub version: String,
}

impl Configuration {
    fn simple_env_lookup(env_var: &'static str,
                         err: EnvVarLookupError)
                         -> Result<String, EnvVarLookupError> {
        env::var(env_var).map_err(|e| match e {
            env::VarError::NotUnicode(s) => EnvVarLookupError::InvalidEnvVariable(env_var, s),
            _ => err,
        })
    }

    pub fn get_port() -> Result<u16, EnvVarLookupError> {
        match env::var("PORT") {
            Ok(s) => s.parse().map_err(EnvVarLookupError::InvalidPortNumber),
            Err(_) => Err(EnvVarLookupError::PortNumberNotProvided),
        }
    }

    pub fn get_application_name() -> Result<String, EnvVarLookupError> {
        Self::simple_env_lookup("APP_NAME", EnvVarLookupError::AppNameNotProvided)
    }

    pub fn get_build_id() -> Result<String, EnvVarLookupError> {
        Self::simple_env_lookup("BUILD_ID", EnvVarLookupError::BuildIdNotProvided)
    }

    pub fn get_version() -> Result<String, EnvVarLookupError> {
        Self::simple_env_lookup("VERSION", EnvVarLookupError::VersionNotProvided)
    }

    pub fn new() -> Result<Configuration, EnvVarLookupError> {
        Ok(Configuration {
            port: Self::get_port()?,
            application_name: Self::get_application_name()?,
            build_id: Self::get_build_id()?,
            version: Self::get_version()?,
        })
    }
}

impl fmt::Display for Configuration {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Configuration\n\tport: {},\n\tapplication_name: {},\n\tbuild_id: {}\n\tversion: \
                {}\n",
               self.port,
               self.application_name,
               self.build_id,
               self.version)
    }
}
